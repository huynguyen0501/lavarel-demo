<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Mockery;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    use     WithFaker;
    use     WithoutMiddleware;
    /** @test  */
    public function update_task_success_test()
    {
        $task = Task::factory()->create();
        $dataUpdate =[
            'name' => $this -> faker ->name,
            'body' => $this ->faker ->text
        ];

        $response = $this->Json('PUT' ,route('tasks.update', $task->id), $dataUpdate);
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test  */
    public function user_can_not_update_task_if_name_is_null()
    {
        $task = Task::factory()->create();
        $dataUpdate =[
            'name' =>'',
        ];

        $response = $this->json('PUT', route('tasks.update', $task->id), $dataUpdate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response -> assertJson(fn (AssertableJson $json) =>
        $json-> has('errors' ,fn (AssertableJson $json)=>
        $json->has('name')
            ->etc()
        ) ->etc()
        );
    }


}
