<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    use     WithoutMiddleware;
    /** @test  */
    public function delete_task_test()
    {
        $task = Task::factory()->create();
        $response = $this->delete(route("tasks.destroy", $task ->id));
        $response->assertRedirect(route('tasks.index'));
    }

}
