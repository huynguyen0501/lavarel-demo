<?php

namespace Tests\Feature\Tasks;

use App\Models\Post;
use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    /** @test  */
    public function user_can_get_all_task(): void
    {
        $task= Task::factory()->create();
        $response = $this->getJson(route('tasks.index'));

        $response->assertStatus(Response::HTTP_OK);
        $response ->assertViewIs('tasks.index');
    }

}
