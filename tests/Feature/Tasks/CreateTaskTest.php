<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateTaskTest extends TestCase
{
    /** @test  */
    public function user_can_view_create(): void
    {
        $response = $this->getJson(route('tasks.create'));

        $response->assertStatus(Response::HTTP_OK);
        $response ->assertViewIs('tasks.create');
    }

}
