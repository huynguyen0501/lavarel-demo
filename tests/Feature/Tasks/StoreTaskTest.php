<?php

namespace Tests\Feature\Tasks;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Mockery;
use Tests\TestCase;

class StoreTaskTest extends TestCase
{
    use     WithoutMiddleware;
//    use WithFaker;
    public   function getCreateTaskRoute(){
        return   route('tasks.store');
}

    /** @test  */

    public function store_task_success_test()
    {

        $faker = \Faker\Factory::create();
        $response = $this->post($this->getCreateTaskRoute(),[
            'name' => $faker->name,
            'content' => $faker->text,
        ]);
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test  */
    public function user_can_not_create_task_if_name_is_null()
    {
        $dataCreate =[
            'name' =>'',
        ];

        $response = $this->postJson($this->getCreateTaskRoute(), $dataCreate);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response -> assertJson(fn (AssertableJson $json) =>
        $json-> has('errors' ,fn (AssertableJson $json)=>
        $json->has('name')
            ->etc()
        ) ->etc()
        );
    }


}
