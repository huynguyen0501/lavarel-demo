<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeletePostTest extends TestCase
{
    /** @test */
    public function user_can_delete_post_if_post_exits(): void
    {
        $post = Post::factory()->create();
        $postCountBeforeDelete = Post::count();

        $response = $this->json('DELETE', route('posts.destroy', $post->id));

        $response->assertStatus(Response::HTTP_OK);

        $this->assertDatabaseMissing('post', ['id' => $post->id]);

        $postCountAfterDelete = Post::count();
        $this->assertEquals($postCountBeforeDelete - 1, $postCountAfterDelete);
    }

}
