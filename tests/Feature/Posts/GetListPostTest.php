<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetListPostTest extends TestCase
{
    /** @test */
    public function user_can_get_list_posts()
    {
//        $dataLogin=[
//            'email'=> 'huy@gmail.com',
//            'password'=>'123456'
//        ];
//        $response_login = $this->postJson(route('login'), $dataLogin);
        $postCount =Post::count();
        $response = $this->getJson(route('posts.index'));

        $response->assertStatus(Response::HTTP_OK);
        $response -> assertJson(fn (AssertableJson $json) =>
        $json  ->has('data', fn (AssertableJson $json) =>
        $json->has ('data')
            ->has('currenpage')
            ->has('perpage')
            ->has('meta ' ,fn (AssertableJson $json)=>
            $json->where('total ', $postCount)
            )
        )
            ->has ('message')
        );
    }
}


