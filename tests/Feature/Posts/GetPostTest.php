<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetPostTest extends TestCase
{
    /** @test */
    public function user_can_get_list_post_if_post_exit()
    {

        $post = Post::factory()->create();

        $response = $this->getJson(route('posts.show', $post ->id));

        $response->assertStatus(Response::HTTP_OK);
        $response -> assertJson(fn (AssertableJson $json) =>
        $json-> has('message')
            ->has('data' ,fn (AssertableJson $json)=>
            $json->where('name', $post->name)
                ->etc()
            )
        );
    }
    /** @test */
    public  function user_can_not_get_list_post_if_post_exit(){
        $postID =-1;
        $response = $this->getJson(route('posts.show', $postID));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
