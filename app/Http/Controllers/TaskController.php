<?php

namespace App\Http\Controllers;
use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdatePutTaskRequest;
use App\Http\Requests\UpdateTaskRequest;
use App\Http\Resources\TaskResource;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Request;
use App\Models\Task;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Tests\Feature\Tasks\StoreTaskTest;

class TaskController extends Controller
{
    protected $task;

    public function __construct(Task $task)
    {
        $this->task= $task ;
    }

    public function index()
    {
//        $tasks=$this->task->all();
        $tasks = Task::paginate(10);
        return view('tasks.index', compact('tasks'));
    }
    public function store(  StoreTaskRequest $request)
    {
         $task=$this->task->create($request->all());
        return redirect()->route('tasks.index');

    }
    public function create(){
        return \view('tasks.create');
    }
    public function update(  UpdateTaskRequest $request, $id){
        $task =$this->task-> findOrFail($id);
        $task->update($request->all());
        return redirect()->route('tasks.index');
    }
    public function destroy( $id){
        $this->task->destroy($id);
        return redirect()->route('tasks.index');

    }
    public function edit($id ){
        $task=$this->task->findOrFail($id);
        return \view('tasks.edit', compact('task'));
    }
}
