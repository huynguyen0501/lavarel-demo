<?php

namespace App\Http\Controllers;

use http\Message;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

   public function senSucessResponse($data ='', $message ='success', $status =200){
       return \response()->json([
           'data'=>$data,
           'message'=>$message,
       ],$status);

   }
}
