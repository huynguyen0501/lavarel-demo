<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostRource;
use App\Models\Post;

class PostController extends Controller
{
    protected $post;
    public function __construct(Post $post)
    {
        $this->post=$post;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $posts =$this->post->paginate(5);
        $postCollection= new PostCollection($posts);

        return $this-> senSucessResponse($postCollection , 'success', \Illuminate\Http\Response::HTTP_OK);
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store( StorePostRequest $request)
    {
        $dataCreat= $request ->all();
        $post = $this -> post -> create($dataCreat);
        $postResource = new PostRource($post);
        return $this-> senSucessResponse($postResource , 'success', \Illuminate\Http\Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $post = $this -> post -> findOrFail($id);
        $postResource = new PostRource($post);
        return $this-> senSucessResponse($postResource, 'success', \Illuminate\Http\Response::HTTP_OK);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update( UpdatePostRequest $request, string $id)
    {
        $post = $this -> post -> findOrFail($id);
        $dataUpdate =$request -> all();
        $post ->update($dataUpdate);
        $postResource = new PostRource($post);
        return $this-> senSucessResponse($postResource, 'update success', \Illuminate\Http\Response::HTTP_OK);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $post = $this -> post -> findOrFail($id);
        $post ->delete();
        $postResource = new PostRource($post);
        return $this-> senSucessResponse($postResource, 'delete success', \Illuminate\Http\Response::HTTP_OK);
    }
}
