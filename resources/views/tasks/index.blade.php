@extends('layouts.app')

@section('content') <!-- Provide a section name, in this case, 'content' -->
<div class="container">
    <div class="row justify-content-center" >
        <a href="{{ route('tasks.create') }}" class="btn   btn-dark" >create</a>
        <table class="table table-striped">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Content</th>
            </tr>
            @foreach($tasks as $task)
                <tr>
                    <td>{{ $task->id }}</td>
                    <td>{{ $task->name }}</td>
                    <td>{{ $task->content }}</td>
                    <td>
                        <form method='post' action="{{route('tasks.destroy', $task->id)}}">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger"> Delete</button>
                            <a href="{{route('tasks.edit' , $task->id)}}" class="btn btn-warning"> Edit </a>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
        {{$tasks->links()}}
    </div>
</div>
@endsection
