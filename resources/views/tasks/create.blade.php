@extends('layouts.app')

@section('content') <!-- Provide a section name, in this case, 'content' -->
<div class="container">
    <div class="row justify-content-center" >
        <div class="col-md-8">
            <h2> Create-Task</h2>
            <form action="{{route('tasks.store')}}" method="POST">
                @csrf
                <input type="text" class="form-group" name="name" placeholder="Name..." >
                <input type="text" class="form-group" name="content" placeholder="Content...">
                <button class = "btn btn-success" > Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
