@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Dashboard') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        {{ __('You are logged in!') }}
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Logout') }}
                                </button>
{{--                                <button type="tasks" class="btn btn-primary">--}}
{{--                                    {{ route('tasks.index}}--}}
{{--                                </button>--}}
                            </form>
                            <a href="{{route('tasks.index')}}">tasks</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
